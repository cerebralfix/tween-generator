var vmApp = new Vue({
	el: "#app",
	data: {
		gameWidth: 800,
		gameHeight: 600,
		uploadError: "",
		items: [],
		spriteToTween: 0,
		moveTween: false,
		rotateTween: false,
		alphaTween: false,
		scaleTween: false,
		forever: false,
		easings: easings,
		easingStyles: easingStyles.slice(0, 3),
		numberAdded: 0,
		mouseX: 0,
		mouseY: 0,
		relativeTo: "absolute",
	},
	methods: {
		deleteAsset: function(index) {
			this.items[index].sprite.destroy();
			this.items.splice(index, 1);
			if (this.spriteToTween == this.items.length) {
				this.spriteToTween = this.items.length - 1;
			}
		},

		addImage: function(targetFile) {
			if (targetFile !== "") {
				var formData = new FormData();
				formData.append("file", targetFile);

				fetch("/uploadfile", {
					method: "POST",
					body: formData,
				});

				var fileName = targetFile.name;

				this.items.push(new Item(fileName, "img" + this.numberAdded));
				this.spriteToTween = this.items.length - 1;
				this.numberAdded++;

				var input = document.getElementById("assetUpload");
				input.value = "";
				input.dispatchEvent(new Event("change"));
			} else {
				this.uploadError = "cannot upload file: no file specified";
			}
		},

		fileSelected: async function(e) {
			if (e.target.value !== "") {
				this.addImage(e.target.files[0]);
				await this.addAssets();
			}
		},

		updateStyles: function() {
			if (this.getTween().easingId === 6) {
				this.easingStyles = easingStyles.slice(-1);
				this.getTween().easingStyleId = 3;
			} else {
				this.easingStyles = easingStyles.slice(0, 3);
				this.getTween().easingStyleId = 2;
			}
		},

		updateToggle: function(name) {
			var element = document.getElementById("collapse" + name);
			on = element.className.includes("show");
			var chkbox = document.getElementById(name.toLowerCase() + "Tween");
			chkbox.checked = !on;
			eval("this." + name.toLowerCase() + "Tween = chkbox.checked");
			this.addTweens();
		},

		setForever: function(event) {
			document.getElementById("repeat").hidden = event.target.checked;
			this.getTween().repeat = this.forever ? -1 : 0;
		},

		updateGameSize: function() {
			this.getGameFrame().setAttribute("src", "./game/game.html");
			window.setTimeout(this.startGame, 1000);
			this.items.forEach(function(item) {
				delete item.sprite;
			});
			window.setTimeout(this.addAssets, 1200);
		},

		startGame: function() {
			const src = `game = new Phaser.Game(${this.gameWidth}, ${
				this.gameHeight
			}, Phaser.CANVAS, 'game-div', { create: create });
								function create() { game.input.addMoveCallback(p,this); }
								function p(pointer) { parent.vmApp.updateCoords(game.input.x, game.input.y); }`;
			this.getGameFrame().contentWindow.postMessage(src, "*");
		},

		testConfig: async function() {
			this.addTweens();
		},

		loadAssets: async function(game) {
			const loader = new Phaser.Loader(game);
			this.items.forEach(function(item) {
				if (!item.sprite) {
					loader.image(item.key, "assets/" + item.name);
				}
			});
			await this.load(loader);
		},

		addAssets: async function() {
			var innerGame = this.getGame();
			await this.loadAssets(innerGame);

			this.items.forEach(function(item) {
				if (!item.sprite) {
					item.sprite = innerGame.add.sprite(
						parseInt(item.tween.startPos.x),
						parseInt(item.tween.startPos.y),
						item.key,
					);
				}
			});
		},

		load: async function(pLoader) {
			return new Promise((resolve, reject) => {
				pLoader.onLoadComplete.addOnce(resolve);
				pLoader.start();
			});
		},

		editPos: function(e, index, xy, type) {
			var value = parseFloat(e.target.innerText);
			var item = this.items[index];
			if (!eval("item." + type)) {
				this.addPos(item, type);
			}
			if (!isNaN(value)) {
				eval(`item.${type}.${xy} = value`);
				this.addTweens();
			} else {
				if (eval(`item.${type}.${xy}`)) {
					eval(`e.target.innerText = item.${type}.${xy}`);
				} else {
					e.target.innerText = "?";
				}
			}
		},

		addPos: function(item, type) {
			eval(`item.${type} = {}`);
		},

		addTweens: function() {
			this.resetSprite();
			var tween = this.getTween();
			var sprite = this.getItem().sprite;
			var properties = {};
			var scaleProperties = {};
			var valid = false;
			var validScale = false;
			var game = this.getGame();

			if (this.moveTween) {
				if (tween.endPos) {
					valid = this.getProperties(tween.endPos, properties);
				}
			}
			if (this.rotateTween) {
				if (tween.endAngle !== 0) {
					properties.angle = parseInt(tween.endAngle);
					valid = true;
				}
			}

			if (this.scaleTween) {
				if (tween.endScale) {
					validScale = this.getProperties(tween.endScale, scaleProperties);
				}
			}

			if (this.alphaTween) {
				properties.alpha = parseFloat(tween.endAlpha);
				valid = true;
			}

			if (valid) {
				tween.transitionTween = game.add
					.tween(sprite)
					.to(
						properties,
						tween.duration,
						eval(tween.getEasing()),
						true,
						parseInt(tween.delay),
						this.getRepeat(tween.repeat),
						tween.yoyo,
					);
			}
			if (validScale) {
				tween.scaleTween = game.add
					.tween(sprite.scale)
					.to(
						scaleProperties,
						tween.duration,
						eval(tween.getEasing()),
						true,
						tween.delay,
						this.getRepeat(tween.repeat),
						tween.yoyo,
					);
			}

			this.focusGame();
		},

		resetSprite() {
			var item = this.getItem();
			var tween = this.getTween();
			item.sprite.x = tween.startPos.x;
			item.sprite.y = tween.startPos.y;
			item.sprite.angle = tween.startAngle;
			item.sprite.alpha = tween.startAlpha;
			item.sprite.scale.x = tween.startScale.x;
			item.sprite.scale.y = tween.startScale.y;
			if (tween.transitionTween) {
				this.getGame().tweens.remove(tween.transitionTween);
				tween.transitionTween = null;
			}
			if (tween.scaleTween) {
				this.getGame().tweens.remove(tween.scaleTween);
				tween.scaleTween = null;
			}
		},

		getGameFrame: function() {
			return document.getElementById("game-frame");
		},

		getGame: function() {
			return this.getGameFrame().contentWindow.game;
		},

		focusGame: function() {
			this.getGameFrame().contentWindow.focus();
		},

		getProperties: function(from, properties) {
			var valid = false;
			if ("?" !== from.x) {
				properties.x = from.x;
				valid = true;
			}
			if ("?" !== from.y) {
				properties.y = from.y;
				valid = true;
			}
			return valid;
		},

		getRepeat: function(repeat) {
			if (this.forever) {
				return -1;
			}
			return parseInt(repeat);
		},

		updateCoords: function(x, y) {
			this.mouseX = x;
			this.mouseY = y;
		},

		getItem: function() {
			return this.items[this.spriteToTween];
		},

		getTween: function() {
			return this.getItem().tween;
		},

		hasChanged: function(tween) {
			if (tween.startPos.x != 0 || tween.startPos.y != 0 || tween.endPos) {
				return true;
			}
			if (tween.startScale.x != 1 || tween.startScale.y != 1 || tween.endScale) {
				return true;
			}
			if (tween.startAlpha != 1 || tween.endAlpha != 1) {
				return true;
			}
			if (tween.startAngle != 0 || tween.endAngle != 0) {
				return true;
			}
			return false;
		},

		setPivot: function(item) {
			item.sprite.pivot.set(item.pivot.x, item.pivot.y);
		},

		setAnchor: function(item) {
			item.sprite.anchor.set(item.anchor.x, item.anchor.y);
		},
	},
	computed: {
		tweenedItems: function() {
			return this.items.filter((item) => this.hasChanged(item.tween));
		},
	},
	mounted: function() {
		window.setTimeout(this.startGame, 1000);
	},
});
