const easings = [
	{ id: 0, name: "Back" },
	{ id: 1, name: "Bounce" },
	{ id: 2, name: "Circular" },
	{ id: 3, name: "Cubic" },
	{ id: 4, name: "Elastic" },
	{ id: 5, name: "Exponential" },
	{ id: 6, name: "Linear" },
	{ id: 7, name: "Quadratic" },
	{ id: 8, name: "Quartic" },
	{ id: 9, name: "Quintic" },
	{ id: 10, name: "Sinusoidal" },
];

const easingStyles = [
	{ id: 0, name: "In" },
	{ id: 1, name: "Out" },
	{ id: 2, name: "InOut" },
	{ id: 3, name: "None" },
];

class Helper {
	static getValue(relativeTo, value, dimension) {
		if (relativeTo !== "absolute") {
			if (value != 0) {
				return `game.${relativeTo} * ${(value / dimension).toFixed(4)}`;
			}
		}
		return value;
	}
}

class Tween {
	startPos = { x: 0, y: 0 };
	endPos = null;
	startScale = { x: 1, y: 1 };
	endScale = null;
	startAngle = 0;
	endAngle = 0;
	startAlpha = 1;
	endAlpha = 1;
	easingId = 10;
	easingStyleId = 2;
	duration = 1000;
	delay = 0;
	repeat = 0;
	yoyo = false;
	autoStart = true;
	transitionTween = null;
	scaleTween = null;

	getCode(relativeTo, gameWidth, gameHeight) {
		let code = "";
		let properties = {};
		let scaleProperties = {};
		let tween = false;
		let scale = false;
		let dimension = 1;
		gameWidth = parseInt(gameWidth);
		gameHeight = parseInt(gameHeight);

		if (relativeTo == "width") {
			dimension = gameWidth;
		}
		if (relativeTo == "height") {
			dimension = gameHeight;
		}
		code = `sprite.x = ${Helper.getValue(relativeTo, this.startPos.x, dimension)};
		sprite.y = ${Helper.getValue(relativeTo, this.startPos.y, dimension)};
		sprite.angle = ${this.startAngle};
		sprite.alpha = ${this.startAlpha};
		sprite.scale = {x: ${this.startScale.x}, y: ${this.startScale.y}};
		`;

		if (this.endPos) {
			if (this.endPos.x && this.endPos.x != this.startPos.x) {
				properties.x = Helper.getValue(relativeTo, this.endPos.x, dimension);
				tween = true;
			}
			if (this.endPos.y && this.endPos.y != this.startPos.y) {
				properties.y = Helper.getValue(relativeTo, this.endPos.y, dimension);
				tween = true;
			}
		}
		if (this.endAngle != this.startAngle) {
			properties.angle = this.endAngle;
			tween = true;
		}
		if (this.endAlpha != this.startAlpha) {
			properties.alpha = this.endAlpha;
			tween = true;
		}
		if (tween) {
			code = `${code} this.game.add.tween(sprite).to(${this.getPropertiesString(properties)}, ${
				this.duration
			}, ${this.getEasing()}, ${this.autoStart}, ${this.delay}, ${this.repeat}, ${this.yoyo}); \n `;
		}
		if (this.endScale !== null) {
			if (this.endScale.x && this.endScale.x != this.startScale.x) {
				scaleProperties.x = this.endScale.x;
				scale = true;
			}
			if (this.endScale.y && this.endScale.y != this.startScale.y) {
				scaleProperties.y = this.endScale.y;
				scale = true;
			}
			if (scale) {
				code = `${code} \n this.game.add.tween(sprite.scale).to(${this.getPropertiesString(
					scaleProperties,
				)}, ${this.duration}, ${this.getEasing()}, ${this.autoStart}, ${this.delay}, ${this.repeat}, ${
					this.yoyo
				});`;
			}
		}
		return code;
	}

	getPropertiesString(properties) {
		var propString = "";
		const sep = ", ";
		for (let [key, value] of Object.entries(properties)) {
			propString += `${key}:${value}, `;
		}
		if (propString !== "") {
			return `{${propString.slice(0, propString.length - 2)}}`;
		}
		return "";
	}

	getEasing() {
		return `Phaser.Easing.${easings[this.easingId].name}.${easingStyles[this.easingStyleId].name}`;
	}
}

class Item {
	name = "";
	key = "";
	sprite = null;
	anchor = { x: 0, y: 0 };
	pivot = { x: 0, y: 0 };

	tween = new Tween();

	constructor(name, key) {
		this.name = name;
		this.key = key;
	}

	getCode(relativeTo, gameWidth, gameHeight) {
		let dimension = 1;
		if (relativeTo == "width") {
			dimension = gameWidth;
		}
		if (relativeTo == "height") {
			dimension = gameHeight;
		}

		return `sprite.anchor.set(${this.anchor.x}, ${this.anchor.y});
		sprite.pivot.set(${Helper.getValue(relativeTo, this.pivot.x, dimension)}, ${Helper.getValue(
			relativeTo,
			this.pivot.y,
			dimension,
		)});
		${this.tween.getCode(relativeTo, gameWidth, gameHeight)}`;
	}
}
