// call all the required packages
const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
const path = require("path");

// CREATE EXPRESS APP
const app = express();
let base = path.dirname(__dirname);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(base, "client")));

// ROUTES WILL GO HERE
app.get("/", function(req, res) {
	res.sendFile(path.join(base, "client", "index.html"));
});
// SET STORAGE
var storage = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, path.join(base, "client", "assets"));
	},
	filename: function(req, file, cb) {
		cb(null, file.originalname);
	},
});

var upload = multer({ storage: storage });

app.post("/uploadfile", upload.single("file"), (req, res, next) => {
	const file = req.file;
	if (!file) {
		const error = new Error("Please upload a file");
		error.httpStatusCode = 400;
		return next(error);
	}
	res.send(file);
});
app.listen(8080, () => console.log("Server started on port 8080"));
