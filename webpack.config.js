var CopyWebpackPlugin = require("copy-webpack-plugin");
var path = require("path");

module.exports = {
	context: path.resolve(__dirname),
	output: {
		path: path.resolve(__dirname),
		filename: "junk.js",
	},
	plugins: [
		new CopyWebpackPlugin([
			{ from: "./src/run.bat", to: "dist/" },
			{ from: "./src/client/index.html", to: "dist/client/" },
			{ from: "./src/client/game/game.html", to: "dist/client/game/" },
			{ from: "./src/client/js", to: "dist/client/js" },
			{ from: "./src/client/assets", to: "dist/client/assets" },
			{ from: "./src/client/lib", to: "dist/client/lib" },
			{ from: "./src/server", to: "dist/server" },
			{ from: "./node_modules/phaser-ce/build/phaser.min.js", to: "dist/client/lib" },
		]),
	],
};
