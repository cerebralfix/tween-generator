# CerebralFix Tween Generator

Simple HTML/Javascript/Vue.js app to test Phaser tween configuration and output the code to reproduce the tween.
1: Upload one or more images, each image will be automatically added into the Phaser game on the page. Images are uploaded to /dist/assets
2: Select an image to tween from the drop-down box
3: Configure which tween(s) you want applied to the image. Tween will be immediately applied as settings change
4: Configure easing, duration, delay etc
5: Click on 'code' tab to view the code to copy into your game.

# Build Instructions

These instructions assume you have [nodejs v8](https://nodejs.org/en/download/) or above installed.

## Install Dependencies

Run `npm install` to install all required dependencies.
This must be run before running any other commands.

## Build

Run `npm run build` to trigger a full rebuild of the game.
This will output a deployable build of the game into the `dist` directory.

## Start

`npm run start`
This step starts the server defined in server.js

Server runs on http://localhost:8080
